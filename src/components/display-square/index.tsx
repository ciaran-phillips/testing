import { Component } from "preact";
import { h } from "preact"; /** @jsx h */

import { Square, SquareValue } from '../game/boards.types';

interface Props {
  square: Square;
  onClick: (e: Event) => void;
}

export class DisplaySquare extends Component<Props> {
  private renderValue(value: SquareValue): string {
    if (value === SquareValue.EMPTY) {
      return '';
    }
    return value;
  }

  public render(): JSX.Element {
    const { square, onClick } = this.props;
    const squareClass = square.inWinningLine ? 'square square--highlight' : 'square';
    return (
      <div>
        <button onClick={onClick} class={squareClass}>
          {this.renderValue(square.value)}
        </button>
      </div>
    )
  }
}