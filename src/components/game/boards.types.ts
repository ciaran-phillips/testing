export enum SquareValue {
  X = 'X',
  Y = 'Y',
  EMPTY = 'EMPTY'
}

export type Player = SquareValue.X | SquareValue.Y;

export interface Square {
  value: SquareValue;
  inWinningLine: boolean;
}

export interface BoardState { 
  squares: Square[][];
  winner: Player | null;
  nextPlayer: Player;
};

type WinningLine = {
  row: number;
  col: number;
}[];

export class GameLogic {
  public static initialiseBoardState(sideLength: number): BoardState {
    return {
      squares: Array(sideLength).fill(null)
        .map(_ => Array(sideLength).fill(this.newSquare())),
      winner: null,
      nextPlayer: SquareValue.X
    }
  }

  public static isSquareEmpty(boardState: BoardState, row: number, col: number): boolean {
    return boardState[row][col] === SquareValue.EMPTY;
  }

  public static markSquare(boardState: BoardState, row: number, col: number): BoardState {
    const alreadyFinished = boardState.winner !== null;
    const nonEmpty = boardState.squares[row][col].value !== SquareValue.EMPTY;
    if (alreadyFinished || nonEmpty) {
      return boardState;
    }

    const newState = this.copyState(boardState);
    const currentPlayer = boardState.nextPlayer;
    newState.squares[row][col].value = boardState.nextPlayer;
    newState.nextPlayer = currentPlayer === SquareValue.X ? SquareValue.Y : SquareValue.X;

    const winningLine = this.findWinningLine(newState, row, col);
    if (winningLine !== null) {
      newState.winner = currentPlayer;
      this.markWinningLine(newState, winningLine);
    }

    return newState;
  }

  public static findWinningLine(boardState: BoardState, row: number, col: number): WinningLine | null {
    return this.getWinningLineFromRow(boardState, row) ||
      this.getWinningLineFromCol(boardState, col);
  }

  private static markWinningLine(boardState: BoardState, line: WinningLine): void {
    line.forEach(coord => {
      boardState.squares[coord.row][coord.col].inWinningLine = true;
    });
  }

  private static getWinningLineFromRow(boardState: BoardState, row: number): WinningLine | null {
    const rowValues = boardState.squares[row].map(square => square.value);
    if (this.lineValuesMatch(rowValues)) {
      return rowValues.map((_, index) => ({
        row,
        col: index
      }))
    }
    return null;
  }

  private static getWinningLineFromCol(boardState: BoardState, col: number): WinningLine | null {
    const colValues = boardState.squares.reduce<SquareValue[]>((colValues, row) => { 
      colValues.push(row[col].value);
      return colValues;
    }, []);

    if (this.lineValuesMatch(colValues)) {
      return colValues.map((_, index) => ({
        col,
        row: index
      }))
    }
    return null;
  }

  private static lineValuesMatch(values: SquareValue[]): boolean {
    const allEqual = values.every(val => val === values[0]);
    return allEqual && values[0] !== SquareValue.EMPTY;
  }

  private static copyState(state: BoardState): BoardState {
    return {
      winner: state.winner,
      squares: state.squares.map(row => row.map(square => ({ ...square }))),
      nextPlayer: state.nextPlayer
    };
  }

  private static newSquare(): Square {
    return {
      value: SquareValue.EMPTY,
      inWinningLine: false
    }
  }
}