import { h } from "preact"; /** @jsx h */

import { Component } from "preact";
import { BoardState, GameLogic, SquareValue } from './boards.types';
import { DisplaySquare } from '../display-square';

export class Game extends Component {
  private readonly BOARD_SIZE = 3;

  state: BoardState;

  constructor() {
    super();

    this.state = GameLogic.initialiseBoardState(this.BOARD_SIZE);
  }

  public handleClick(row: number, col: number): void {
    const newState = GameLogic.markSquare(this.state, row, col);
    this.setState(newState);
  }

  public render(): JSX.Element {
    const squares = this.state.squares;
    return (
      <div>
        <div class="board">
          {squares.map((row, rowInd) => (
            <div class="row">
              {row.map((square, colInd) => (
                <DisplaySquare square={square} onClick={() => this.handleClick(rowInd, colInd)}></DisplaySquare>
              ))}
            </div>
          ))}
        </div>

        <h2>Winner: {this.state.winner}</h2>
      </div>
      
    );
  }
}