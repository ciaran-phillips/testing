# Julius Baer Markets Toolbox Team Hiring Candidate

We would like you to do all the work needed on a new branch (name of your choice, but let us know).

The project has been configured with [parcel](https://github.com/parcel-bundler/parcel) and makes use of [preact](https://github.com/preactjs/preact) (v8) while [karma](https://github.com/karma-runner/karma) is also available for testing.

`npm start` will fire a local server and allow you to use a browser to see what are you developing.
`npm test` will run all the tests that you have written, if any

The language of choice is typescript. If you feel like another library is needed, feel free to add it but please document the reason you are using it.

## What we are expecting

The target is to create a tic-tac-toe game.

We are expecting a board with nine fields and the ability to click on a field to display the corresponding marker (either an `X` or an `O`).
We assume that players are using the same mouse and do play in the correct order so that first click will be an `x`, the second an `o` and so on.

In the case of a winner a message should be displayed and no further input (mouse clicks) should be allowed by the user.
In the case of a draw (no further spots left to fill) a message should be displayed again.

As soon as you are done with the implementation and there is some time left, please try to make the board look better. You should be using plain css for that.
